package ee.cake.models;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Order {
	
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String customerName;
    private BigDecimal price;
    private String statusCode;

    protected Order() {}
    public Order(String customerName, BigDecimal price, String statusCode) {
        this.customerName = customerName;
        this.price = price;
        this.statusCode = statusCode;
    }

    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	@Override
    public String toString() {
        return String.format(
                "Order[id=%d, customerName='%s', price='%s', statusCode='%s']",
                id, customerName, price, statusCode);
    }

}
