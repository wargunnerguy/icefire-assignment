package ee.cake.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ee.cake.models.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {

	List<Order> findByName(String name);
}