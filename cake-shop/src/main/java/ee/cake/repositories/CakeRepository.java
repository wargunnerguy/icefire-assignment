package ee.cake.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import ee.cake.models.Cake;

public interface CakeRepository extends CrudRepository<Cake, Long> {

	List<Cake> findByCakeName(String cakeName);
}